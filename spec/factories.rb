# frozen_string_literal: true

FactoryBot.define do
  sequence(:id) { |n| n }
  sequence(:minor) { |n| n }
  sequence(:sha) { SecureRandom.hex(20) }
  sequence(:created_at, aliases: %w[updated_at finished_at]) do
    Time.now.utc.iso8601
  end

  factory :auto_deploy_ref, class: 'ReleaseTools::AutoDeploy::Version' do
    major { 42 }
    minor { generate(:minor) }
    patch { Time.now.strftime('%Y%m%d%H') }
    gitlab_sha { generate(:sha) }
    omnibus_sha { generate(:sha) }

    initialize_with { new("#{major}.#{minor}.#{patch}-#{gitlab_sha[0, 11]}.#{omnibus_sha[0, 11]}") }
  end

  factory :rspec_double, aliases: [:gitlab_response], class: 'RSpec::Mocks::Double' do
    skip_create

    initialize_with do
      new("Gitlab::ObjectifiedHash", **attributes)
    end
  end

  factory :comparison, aliases: [:compare], parent: :gitlab_response do
    commit { nil }
    commits { [] }
    compare_timeout { false }
    diffs { [] }
    web_url { "https://example.com/foo/bar/compare/foo...bar" }
  end

  factory :deployment, parent: :gitlab_response do
    id
    iid { id }
    ref { 'master' }
    sha
    status { 'success' }
    created_at
    updated_at { created_at }

    trait(:success)
    trait(:failed) { status { 'failed' } }
    trait(:running) { status { 'running' } }

    # Omnibus deployments are on auto-deploy tags
    factory :omnibus_deployment do
      ref { build(:auto_deploy_ref, omnibus_sha: sha) }
    end
  end

  factory :issue, parent: :gitlab_response do
    id
    iid { id }
    state { 'opened' }
    assignees { [] }
    labels { [] }
    web_url { "https://example.com/foo/bar/-/issues/#{iid}" }

    trait(:closed) { state { 'closed' } }
    trait(:opened) { state { 'opened' } }
  end

  factory :merge_request, parent: :gitlab_response do
    id
    iid { id }
    state { 'opened' }
    source_branch { 'feature-branch' }
    target_branch { 'master' }
    assignees { [] }
    labels { [] }
    web_url { "https://example.com/foo/bar/-/merge_requests/#{iid}" }

    trait(:closed) { state { 'closed' } }
    trait(:locked) { state { 'locked' } }
    trait(:merged) { state { 'merged' } }
    trait(:opened) { state { 'opened' } }
  end

  factory :pipeline, parent: :gitlab_response do
    id
    status { 'created' }
    ref { 'new-pipeline' }
    sha
    web_url { "https://example.com/foo/bar/-/pipelines/#{id}" }
    created_at
    updated_at { created_at }

    trait(:created) { status { 'created' } }
    trait(:waiting_for_resource) { status { 'waiting_for_resource' } }
    trait(:preparing) { status { 'preparing' } }
    trait(:pending) { status { 'pending' } }
    trait(:running) { status { 'running' } }
    trait(:success) do
      status { 'success' }
      finished_at
    end
    trait(:failed) do
      status { 'failed' }
      finished_at
    end
    trait(:canceled) { status { 'canceled' } }
    trait(:skipped) { status { 'skipped' } }
    trait(:manual) { status { 'manual' } }
    trait(:scheduled) { status { 'scheduled' } }
  end

  factory :user, parent: :gitlab_response do
    id
    name { 'GitLab User' }
    username { 'gitlab-user' }
  end

  factory :diff, parent: :gitlab_response do
    diff do
      <<~HEREDOC
        --- a/doc/update/5.4-to-6.0.md
        +++ b/doc/update/5.4-to-6.0.md
        @@ -71,4 +71,6 @@
        sudo -u git -H bundle exec rake migrate_keys RAILS_ENV=production
        sudo -u git -H bundle exec rake migrate_inline_notes RAILS_ENV=production
        +sudo -u git -H bundle exec rake gitlab:assets:compile RAILS_ENV=production
        +```### 6. Update config files"
      HEREDOC
    end

    new_path { 'doc/update/5.4-to-6.0.md' }
    old_path { new_path }
    a_mode { nil }
    b_mode { '100644' }
    new_file { false }
    renamed_file { false }
    deleted_file { false }
  end

  factory :component_metadata, class: Hash do
    sha
    version { sha }
    ref { 'master' }
    tag { false }

    initialize_with { attributes.transform_keys(&:to_s) }
  end

  factory :releases_metadata, class: Hash do
    omnibus_gitlab_ee { build(:component_metadata) }
    gitlab_ee { build(:component_metadata) }
    gitaly { build(:component_metadata) }
    gitlab_elasticsearch_indexer { build(:component_metadata) }
    gitlab_pages { build(:component_metadata) }
    gitlab_shell { build(:component_metadata) }
    cng_ee { build(:component_metadata) }
    gitlab_assets_tag { build(:component_metadata) }
    mailroom { build(:component_metadata, sha: nil, version: '1.2.3') }

    initialize_with do
      attributes.transform_keys do |k|
        if k == :gitlab_assets_tag
          k.to_s
        else
          k.to_s.tr('_', '-')
        end
      end
    end
  end

  factory :product_version, class: 'ReleaseTools::ProductVersion' do
    transient do
      auto_deploy_ref do
        build(:auto_deploy_ref,
              omnibus_sha: metadata.dig('releases', 'omnibus-gitlab-ee', 'sha'),
              gitlab_sha: metadata.dig('releases', 'gitlab-ee', 'sha'))
      end
      security { false }
      metadata_commit_id { generate(:sha) }
      metadata do
        {
          'security' => security,
          'releases' => build(:releases_metadata)
        }
      end
    end

    after(:build) do |product_version, evaluator|
      info = ReleaseTools::ProductVersion::MetadataInfo.new(
        commit_id: evaluator.metadata_commit_id,
        content: evaluator.metadata
      )
      product_version.instance_variable_set('@full_metadata', info)
    end

    initialize_with do
      new(auto_deploy_ref)
    end
  end
end
