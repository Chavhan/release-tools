# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Rollback::Comparison do
  let!(:fake_client) { stub_const('ReleaseTools::GitlabClient', spy) }

  def stub_compare(comparison)
    allow(fake_client).to receive(:compare).and_return(comparison)
  end

  def new_version
    build(:product_version)
  end

  describe '#execute' do
    it 'compares from the target to the current deployment' do
      target = new_version
      current = new_version

      expect(fake_client).to receive(:compare).with(
        described_class::PROJECT,
        from: target[ReleaseTools::Project::GitlabEe.metadata_project_name].sha,
        to: current[ReleaseTools::Project::GitlabEe.metadata_project_name].sha
      ).and_return(build(:compare))

      instance = described_class.new(current: current, target: target)
      instance.execute
    end

    it 'considers a diff with migrations as safe' do
      compare = create(
        :compare,
        diffs: [
          { 'new_path' => 'db/migrate/foo.rb', 'new_file' => true },
          { 'new_path' => 'db/migrate/bar.rb', 'new_file' => false }
        ]
      )
      stub_compare(compare)

      check = described_class
        .new(current: new_version, target: new_version)
        .execute

      expect(check).to be_safe
      expect(check.migrations.size).to eq(1)
      expect(check.post_deploy_migrations.size).to eq(0)
    end

    it 'considers a diff with post-deploy migrations as unsafe' do
      compare = create(
        :comparison,
        diffs: [
          { 'new_path' => 'db/post_migrate/foo.rb', 'new_file' => true },
          { 'new_path' => 'db/post_migrate/bar.rb', 'new_file' => false }
        ]
      )
      stub_compare(compare)

      check = described_class
        .new(current: new_version, target: new_version)
        .execute

      expect(check).not_to be_safe
      expect(check.migrations.size).to eq(0)
      expect(check.post_deploy_migrations.size).to eq(1)
    end

    it 'considers a compare timeout as unsafe' do
      compare = create(
        :comparison,
        diffs: [
          { 'new_path' => 'README.md', 'new_file' => true },
          { 'new_path' => 'db/post_migrate/bar.rb', 'new_file' => false }
        ],
        compare_timeout: true
      )
      stub_compare(compare)

      check = described_class
        .new(current: new_version, target: new_version)
        .execute

      expect(check).not_to be_safe
      expect(check.migrations.size).to eq(0)
      expect(check.post_deploy_migrations.size).to eq(0)
    end

    it 'considers an empty diff as unsafe' do
      compare = create(:comparison, diffs: [])

      stub_compare(compare)
      check = described_class
        .new(current: new_version, target: new_version)
        .execute

      expect(check).not_to be_safe
      expect(check.migrations.size).to eq(0)
      expect(check.post_deploy_migrations.size).to eq(0)
    end

    it 'considers a diff otherwise safe' do
      compare = create(
        :comparison,
        diffs: [
          { 'new_path' => 'README.md', 'new_file' => true },
          { 'new_path' => 'db/migrate/foo.rb', 'new_file' => false }
        ]
      )
      stub_compare(compare)

      check = described_class
        .new(current: new_version, target: new_version)
        .execute

      expect(check).to be_safe
    end
  end
end
