# frozen_string_literal: true

# Helpers for working with the slack responses
module SlackHelper
  # Returns a slack markdown block
  #
  # text  - Block content
  def slack_mrkdwn_block(text:, context_elements: [])
    block = [slack_section_block(text: text)]

    if context_elements.empty?
      block
    else
      block << slack_context_block(elements: context_elements)
    end
  end

  def slack_section_block(text:)
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: text
      }
    }
  end

  def slack_header_block(text:)
    {
      type: 'header',
      text: {
        type: 'plain_text',
        text: text
      }
    }
  end

  def slack_divider_block
    { "type": "divider" }
  end

  def slack_context_block(elements: [])
    {
      type: 'context',
      elements: elements
    }
  end
end

RSpec.configure do |config|
  config.include SlackHelper
end
