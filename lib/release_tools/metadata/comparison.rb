# frozen_string_literal: true

module ReleaseTools
  module Metadata
    class Comparison
      include ::SemanticLogger::Loggable

      PROJECTS = [
        Project::OmnibusGitlab, Project::GitlabEe, Project::Gitaly,
        Project::GitlabElasticsearchIndexer, Project::GitlabPages, Project::GitlabShell,
        Project::CNGImage
      ].freeze

      # source and target ProductVersion objects.
      attr_reader :source, :target

      # @param source [ProductVersion] version. This should be the newer version.
      # @param target [ProductVersion] version. This should be the older version.
      def initialize(source:, target:)
        @source = source
        @target = target
      end

      def map_components
        PROJECTS.map do |project|
          yield compare(project), source_sha(project), target_sha(project), project
        end
      end

      def compare(project)
        Retriable.with_context(:api) do
          GitlabClient.compare(project.security_path, from: target_sha(project), to: source_sha(project))
        end
      end

      def source_sha(project)
        source[project.metadata_project_name].sha
      end

      def target_sha(project)
        target[project.metadata_project_name].sha
      end
    end
  end
end
