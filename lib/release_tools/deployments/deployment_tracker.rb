# frozen_string_literal: true

module ReleaseTools
  module Deployments
    # Tracking of deployments using the GitLab API
    class DeploymentTracker
      include ::SemanticLogger::Loggable

      # The deployment statuses that we support.
      DEPLOYMENT_STATUSES = Set.new(%w[running success failed]).freeze

      # A deployment created using the GitLab API
      Deployment = Struct.new(:project_path, :id, :status) do
        def success?
          status == 'success'
        end
      end

      # The name of the staging environment of GitLab.com.
      STAGING = 'gstg'

      # The Metadata Project
      METADATA_PROJECT = Project::Release::Metadata

      # environment - The name of the environment that was deployed to.
      # status - The status of the deployment, such as "success" or "failed".
      # version - The raw deployment version, as passed from the deployer.
      def initialize(environment, status, version)
        @environment = environment
        @status = status
        @version = version
      end

      def qa_commit_range
        unless @environment == STAGING && @status == 'success'
          return []
        end

        current, previous = GitlabClient
          .deployments(
            Project::GitlabEe.auto_deploy_path,
            @environment,
            status: 'success'
          )
          .first(2)
          .map(&:sha)

        [previous, current]
      end

      def track
        logger.info(
          'Recording GitLab deployment',
          environment: @environment,
          status: @status,
          version: @version
        )

        check_status

        version = product_version['gitlab-ee']
        omnibus_version = product_version['omnibus-gitlab-ee']
        gitaly_version = product_version['gitaly']

        log_parsed_versions(
          GitLab: version,
          Omnibus: omnibus_version,
          Gitaly: gitaly_version
        )

        gitlab_deployments = track_gitlab_deployments(version)
        gitaly_deployments = track_gitaly_deployments(gitaly_version)
        omnibus_deployments = track_omnibus_deployments(omnibus_version)

        (gitlab_deployments + gitaly_deployments + omnibus_deployments).compact
      end

      def record_metadata_deployment
        check_status

        begin
          create_product_deployment
        rescue StandardError => ex
          ::Raven.capture_exception(ex)
        end
      end

      private

      def log_parsed_versions(components_version)
        components_version.each do |component, version|
          logger.info(
            "Parsed #{component} version from #{@version}",
            sha: version.sha,
            ref: version.ref,
            is_tag: version.tag?
          )
        end
      end

      def create_product_deployment
        logger.info(
          "Recording Product deployment",
          environment: @environment,
          status: @status,
          version: @version,
          sha: product_version.metadata_commit_id
        )

        Retriable.with_context(:api) do
          GitlabOpsClient.update_or_create_deployment(
            METADATA_PROJECT,
            @environment,
            ref: METADATA_PROJECT.default_branch,
            sha: product_version.metadata_commit_id,
            status: @status
          )
        end
      end

      # Find the first commit that exists in the Security auto-deploy branch and
      # the Canonical default branch
      #
      # The commit being deployed in the auto-deploy branch is usually something
      # that only exists in that branch, and thus can't be used for a Canonical
      # deployment.
      def auto_deploy_intersection(project, sha)
        canonical = GitlabClient
          .commits(project.path, ref_name: project.default_branch, per_page: 100)
          .paginate_with_limit(300)
          .collect(&:id)

        GitlabClient.commits(project.auto_deploy_path, ref_name: sha).paginate_with_limit(300) do |commit|
          return commit.id if canonical.include?(commit.id)
        end

        logger.warn(
          'Failed to find auto-deploy intersection',
          project: project.path,
          sha: sha
        )

        nil
      rescue Gitlab::Error::Error => ex
        logger.warn(
          'Failed to find auto-deploy intersection',
          project: project.path,
          sha: sha,
          error: ex.message
        )

        nil
      end

      def create_deployments(project, ref:, sha:, is_tag:)
        deployments = []

        log_previous_deployment(project.auto_deploy_path)

        logger.info(
          "Recording #{project.name.demodulize} deployment",
          environment: @environment,
          status: @status,
          sha: sha,
          ref: ref
        )

        data =
          Retriable.with_context(:api) do
            GitlabClient.update_or_create_deployment(
              project.auto_deploy_path,
              @environment,
              ref: ref,
              sha: sha,
              status: @status,
              tag: is_tag
            )
          end

        log_new_deployment(project.auto_deploy_path, data)

        deployments << Deployment.new(project.auto_deploy_path, data.id, data.status)

        canonical_sha = auto_deploy_intersection(project, sha)
        return deployments unless canonical_sha

        # Because auto-deploy branches only exist on Security, the deploy above
        # was created there.
        #
        # However, merge requests on Canonical still need to be notified about
        # the deployment, but the given ref won't exist there, so we also create
        # one on Canonical using its default branch and the first commit that
        # exists on both Security and Canonical.
        log_previous_deployment(project.path)

        logger.info(
          "Recording #{project.name.demodulize} Canonical deployment",
          environment: @environment,
          status: @status,
          sha: sha,
          ref: project.default_branch
        )

        data = GitlabClient.update_or_create_deployment(
          project.path,
          @environment,
          ref: project.default_branch,
          sha: canonical_sha,
          status: @status,
          tag: is_tag
        )

        log_new_deployment(project.path, data)

        deployments << Deployment.new(project.path, data.id, data.status)
      end

      def track_gitlab_deployments(version)
        create_deployments(
          Project::GitlabEe,
          ref: version.ref,
          sha: version.sha,
          is_tag: version.tag?
        )
      end

      def track_gitaly_deployments(version)
        create_deployments(
          Project::Gitaly,
          ref: version.ref,
          sha: version.sha,
          is_tag: version.tag?
        )
      end

      def track_omnibus_deployments(version)
        create_deployments(
          Project::OmnibusGitlab,
          ref: version.ref,
          sha: version.sha,
          is_tag: version.tag?
        )
      end

      def log_new_deployment(project_path, deploy)
        logger.info(
          "Created new deployment for #{project_path}",
          path: project_path,
          id: deploy.id,
          iid: deploy.iid,
          ref: deploy.ref,
          sha: deploy.sha
        )
      end

      def log_previous_deployment(project_path)
        deploy = GitlabClient
          .deployments(project_path, @environment, status: 'success')
          .first

        return unless deploy

        logger.info(
          "Previous deployment for #{project_path} is deploy ##{deploy.iid}",
          id: deploy.id,
          iid: deploy.iid,
          ref: deploy.ref,
          sha: deploy.sha
        )
      end

      def check_status
        return if DEPLOYMENT_STATUSES.include?(@status)

        raise ArgumentError, "The deployment status #{@status} is not supported"
      end

      def product_version
        @product_version ||= ProductVersion.from_package_version(@version)
      end
    end
  end
end
