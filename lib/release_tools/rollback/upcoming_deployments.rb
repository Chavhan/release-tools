# frozen_string_literal: true

module ReleaseTools
  module Rollback
    # Verifies if there's a running deployment for a given environment.
    #
    # Verification is done by fetching the running deployments from a specific
    # environment through the API.
    class UpcomingDeployments
      PROJECT = ReleaseTools::Project::GitlabEe

      ENVIRONMENTS = %w[gstg gprd].freeze

      def initialize(environment:)
        @environment = environment
      end

      def any?
        environment? && last_running_deployment.present?
      end

      private

      attr_reader :environment

      def environment?
        ENVIRONMENTS.include?(environment)
      end

      def last_running_deployment
        @last_running_deployment ||= GitlabClient.deployments(
          PROJECT.auto_deploy_path,
          environment,
          status: 'running',
          order_by: 'id',
          sort: 'desc'
        ).paginate_with_limit(1).first
      end
    end
  end
end
