# frozen_string_literal: true

module ReleaseTools
  module Rollback
    # Given a changeset comparison, determines if a rollback is possible
    class Comparison
      include ::SemanticLogger::Loggable

      PROJECT = ReleaseTools::Project::GitlabEe.auto_deploy_path

      # The currently running (or upcoming) AutoDeploy::Version
      attr_reader :current

      # The AutoDeploy::Version to which we want to rollback
      attr_reader :target

      # Array of new migrations in the diff
      attr_reader :migrations

      # Array of new post-deploy migrations in the diff
      attr_reader :post_deploy_migrations

      # current - ProductVersion which is currently running
      # target  - ProductVersion which we want to rollback to
      def initialize(current:, target:)
        @current = current
        @target = target

        @migrations = []
        @post_deploy_migrations = []
      end

      def execute
        @compare = Retriable.with_context(:api) do
          GitlabClient.compare(PROJECT, from: target_rails_sha, to: current_rails_sha)
        end

        @compare.diffs.each do |diff|
          if new_migration?(diff)
            @migrations << diff
          elsif new_post_deploy_migration?(diff)
            @post_deploy_migrations << diff
          end
        end

        logger.info(
          'Rollback comparison',
          safe: safe?,
          migrations: migrations.size,
          post_deploy_migrations: post_deploy_migrations.size,
          empty: empty?,
          timeout: timeout?,
          web_url: web_url
        )

        self
      end

      def safe?
        post_deploy_migrations.none? && !timeout? && !empty?
      end

      def timeout?
        @compare.compare_timeout
      end

      def empty?
        @compare.diffs.empty?
      end

      def web_url
        @compare.web_url
      end

      def current_package
        @current.to_s
      end

      def current_rails_sha
        @current[Project::GitlabEe.metadata_project_name].sha
      end

      def target_package
        @target.to_s
      end

      def target_rails_sha
        @target[Project::GitlabEe.metadata_project_name].sha
      end

      private

      def new_migration?(diff)
        diff['new_path'].start_with?('db/migrate/') && diff['new_file']
      end

      def new_post_deploy_migration?(diff)
        diff['new_path'].start_with?('db/post_migrate/') && diff['new_file']
      end
    end
  end
end
