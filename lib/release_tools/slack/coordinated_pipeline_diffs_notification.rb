# frozen_string_literal: true

module ReleaseTools
  module Slack
    class CoordinatedPipelineDiffsNotification
      include ::SemanticLogger::Loggable

      # @param deploy_version [String] Deployer package version
      # @param environment [String] One of gstg-ref, gstg-cny, gstg, gprd-cny, gprd
      # @param thread_ts [String] The `ts` of the Slack message that this notification
      #        should be threaded to.
      def initialize(deploy_version:, environment:, thread_ts:)
        @deploy_version = deploy_version
        @environment = environment
        @thread_ts = thread_ts
      end

      def execute
        blocks, fallback_text = diff_slack_msg

        logger.info('Sending component diffs notification', deploy_version: deploy_version, environment: environment, fallback_text: fallback_text)

        ReleaseTools::Slack::Message.post(
          channel: ReleaseTools::Slack::ANNOUNCEMENTS,
          message: fallback_text,
          blocks: blocks,
          additional_options: {
            thread_ts: thread_ts
          }
        )
      end

      private

      attr_reader :deploy_version, :environment, :thread_ts

      def diff_slack_msg
        blocks = ::Slack::BlockKit.blocks
        comparison = metadata_comparison

        header_text = header_block(blocks, comparison)

        component_diffs_block(blocks, comparison)

        blocks.divider
        context_block(blocks)

        [blocks.as_json, header_text]
      end

      def metadata_comparison
        source_version = ProductVersion.from_auto_deploy(deploy_version)

        compare_service = Metadata::CompareService.new(
          source: source_version,
          environment: environment
        )

        comparison = compare_service.with_latest_successful_deployment

        # CompareService will flip the source and target if the target is newer than
        # source. Log a message if that happens, since the last deployment should
        # never be newer than deploy_version (except during rollbacks, but rollbacks
        # don't happen in release-tools yet).
        if comparison.source == source_version
          logger.info('Comparing metadata source (deploy_version) and target (last deployment)', source: comparison.source, target: comparison.target)
        else
          logger.fatal('Last deployment is newer than deploy_version', last_deployment_version: comparison.source, deploy_version: deploy_version)
        end

        comparison
      end

      def header_block(blocks, comparison)
        header = "Diff with #{comparison.target} (current version on #{environment})"

        blocks.header(text: header)

        header
      end

      def component_diffs_block(blocks, comparison)
        comparison.map_components do |project_compare, source_sha, target_sha, project|
          diff = diff_text(project_compare, source_sha, target_sha, project)

          blocks.section { |block| block.mrkdwn(text: diff) }
        end
      end

      def diff_text(project_compare, source_sha, target_sha, project)
        if target_sha == source_sha
          commit_link = "<https://gitlab.com/#{project.security_path}/-/commits/#{source_sha}|#{source_sha[0..10]}>"

          "#{project.project_name} - No diff - #{commit_link} (Last running commit)"
        else
          compare_link = "<#{project_compare.web_url}|#{target_sha[0..10]}...#{source_sha[0..10]}>"

          "#{project.project_name} - #{compare_link}"
        end
      end

      def context_block(blocks)
        env = GitlabOpsClient.environments(Project::Release::Metadata.ops_path, name: environment)
        url = "https://ops.gitlab.net/#{Project::Release::Metadata.ops_path}/-/environments/#{env.first.id}"

        blocks.context do |context|
          context.mrkdwn(text: "<#{url}|All #{environment} deployments>")
        end
      end
    end
  end
end
