# frozen_string_literal: true

module ReleaseTools
  module Slack
    module Utilities
      STATUS_ICONS = {
        started: ':ci_running:',
        finished: ':ci_passing:',
        failed: ':ci_failing:',
        unknown: ':warning:'
      }.freeze

      ENVIRONMENT_ICONS = {
        'gstg-ref': ':construction:',
        'gstg-cny': ':hatching_chick:',
        'gstg': ':building_construction:',
        'gprd-cny': ':canary:',
        'gprd': ':party-tanuki:'
      }.freeze

      private

      def deployer_status
        return 'unknown' if pipeline.nil?

        case pipeline.status
        when 'success'
          'finished'
        when 'failed', 'canceled'
          'failed'
        else
          'started'
        end
      end

      def environment_icon
        ENVIRONMENT_ICONS[environment.to_sym]
      end

      def status_icon
        STATUS_ICONS[deployer_status.to_sym]
      end

      def clock_context_element
        { type: 'mrkdwn', text: ":clock1: #{current_time.strftime('%Y-%m-%d %H:%M')} UTC" }
      end

      def current_time
        Time.now.utc
      end
    end
  end
end
