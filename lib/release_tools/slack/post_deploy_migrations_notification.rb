# frozen_string_literal: true

module ReleaseTools
  module Slack
    class PostDeployMigrationsNotification
      include ::SemanticLogger::Loggable
      include Utilities

      def initialize(deploy_version:, pipeline:, environment:)
        @deploy_version = deploy_version
        @pipeline = pipeline
        @environment = environment
      end

      def execute
        logger.info('Sending post-migration slack notification', deploy_version: deploy_version, environment: environment, post_deploy_migration_url: pipeline.web_url, slack_channel: slack_channel)

        return if SharedStatus.dry_run?

        ReleaseTools::Slack::Message.post(
          channel: ReleaseTools::Slack::ANNOUNCEMENTS,
          message: fallback_text,
          blocks: slack_block
        )
      end

      private

      attr_reader :deploy_version, :pipeline, :environment, :slack_channel

      def fallback_text
        "post-deploy migrations #{environment} #{deployer_status} #{deploy_version}"
      end

      def slack_block
        blocks = ::Slack::BlockKit.blocks

        blocks.section do |section|
          section.mrkdwn(text: section_block)
        end

        blocks.context do |block|
          block.mrkdwn(text: clock_context_element[:text])
        end

        blocks.as_json
      end

      def section_block
        [].tap do |text|
          text << environment_icon
          text << status_icon
          text << "*Post-deploy migrations #{environment}*"
          text << "<#{pipeline.web_url}|#{deployer_status}>"
          text << "`#{deploy_version}`"
        end.join(' ')
      end
    end
  end
end
