# frozen_string_literal: true

module ReleaseTools
  module Promotion
    module Checks
      # IncidentRollback checks rollback availability during an incident
      class IncidentRollback
        include ::ReleaseTools::Promotion::Check

        def fine?
          comparison&.safe?
        end

        # Unsafe to rollback isn't necessarily a show-stopper
        def failure_icon
          ':warning:'
        end

        def to_issue_body
          if comparison.nil?
            return "#{icon(self)} Unable to determine rollback availability."
          end

          text = StringIO.new

          text.puts("#{icon(self)} Rollback of `gprd` is #{fine? ? 'available' : 'unavailable'}:")
          text.puts
          text.puts(presenter.present.join("\n"))

          text.string
        end

        private

        def comparison
          @comparison ||= Rollback::CompareService
            .new(current: 'gprd', target: 'gprd')
            .execute
        rescue ArgumentError
          nil
        end

        def upcoming_deployment
          Rollback::UpcomingDeployments.new(environment: 'gprd')
        end

        def presenter
          @presenter ||= Rollback::Presenter.new(comparison, upcoming_deployment)
        end
      end
    end
  end
end
